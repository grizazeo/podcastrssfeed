# README #

Simplemente clona o descarga el repositorio y accede al archivo index.html

### Para que es el repositorio? ###

* Es una aplicación SPA que sirve para leer feeds rss de podcasts, listar los elementos y escucharlos.
* Version 1.2

### Dependencias ###

* Las siguientes librerías se cargan por CDN.
* * Materialize http://materializecss.com/
* * Jquery https://code.jquery.com/jquery-3.1.1.min.js
* * AngularJS https://ajax.googleapis.com/ajax/libs/angularjs/1.5.5/angular.min.js

* Para el parseo de la información del RSS se utiliza la librería rss2json https://rss2json.com