window.google =  { feeds : {} };

window.google.feeds.Feed = function(rssUrl,options){
    options = options || {};
    var apiEndpoint = 'https://api.rss2json.com/v1/api.json';
    var head = document.head || document.getElementsByTagName( 'head' )[0];
    var itemsCount;

    var formatResults = function(data,cb){
        var googleFormat = {feed : {}};
        var _itemsCount = (!isNaN(itemsCount) ? itemsCount : undefined);

        if(data.status == 'ok'){
            googleFormat.feed.feedUrl = data.feed.url;
            googleFormat.feed.title = data.feed.title;
            googleFormat.feed.link = data.feed.link;
            googleFormat.feed.description = data.feed.description;
            googleFormat.feed.author = data.feed.author;
            googleFormat.feed.entries = [];

            if(typeof _itemsCount == 'undefined'){
                _itemsCount = data.items.length;
            }

            for(var i in data.items){
                var item = data.items[i];

                if(_itemsCount <= 0){
                    break;
                }
                _itemsCount--;
                //console.log(item);
                googleFormat.feed.entries.push({
                    title : item.title,
                    link : item.link,
                    content : item.content,
                    contentSnippet : item.content,
                    publishedDate : item.pubDate,
                    categories : item.categories,
                    media:  item.enclosure

                });
            }
            cb(googleFormat);

        }else{
            cb({
                error : {
                    code : 0,
                    message : data.message
                }
            })
        }

    };

    var createScriptTag = function(){
        var s = document.createElement('script');
        s.type = 'text/javascript';

        return s;
    }

    var cleanScriptTag = function(script,callbackName){
        head.removeChild(script);
        script = null;
        delete window[callbackName];
    }

    var generateRandomName = function(){
        var randomName = '_cb_' + (new Date() * 1) + '' + Math.round(Math.random() * 1e3);

        return randomName;

    }

    var jsonpRequest = function(callback){

        var callbackName, script , scriptUrl;

        callbackName = generateRandomName();
        scriptUrl = apiEndpoint + '?rss_url=' + encodeURIComponent(rssUrl);

        for (var i in options) {
            scriptUrl += '&' + encodeURIComponent(i) + '=' + encodeURIComponent(options[i]);
        };

        scriptUrl +=  '&callback=' + callbackName;

        script = createScriptTag();
        script.src = scriptUrl;



        //handle errors

        script.onerror = function(oError){
            cleanScriptTag(script,callbackName);
            callback({
                error : {
                    code : 0,
                    message : "The script " + oError.target.src + " is not accessible."
                }
            })
        }

        window[callbackName] = function( data ) {
            var format = formatResults(data,callback);
            cleanScriptTag(script,callbackName);
        }


        head.appendChild(script);
    }




    return {
        load : function(cb){
            jsonpRequest(cb);
        },
        setNumEntries : function(count){
            itemsCount = count;
        },
        includeHistoricalEntries : function(){
            //todo
        },
        setResultFormat : function(){
            //todo
        },
        findFeeds : function(q,cb){
            return cb({
                code : 0,
                message : 'this method is not available yet'
            });
        }
    }

}

window.google.feeds.Feed.JSON_FORMAT = 'json';
window.google.feeds.Feed.XML_FORMAT = 'xml';
window.google.feeds.Feed.MIXED_FORMAT = 'mixed';
window.google.setOnLoadCallback =  function(cb){cb();}
window.google.load = function(a,b,cb){
    if(cb && cb.callback){
        cb.callback();
    }
}
