
var app=angular.module('PodcastFeedApp',['ngSanitize', 'ui.materialize']);
app.controller("PodcastFeedController",function($sce,$http, $scope){
  $scope.podcasts=[];
  $scope.showResults = true;
  //$scope.podcastUrl= "http://www.sueldo30.com/feed/podcast/";
  //genera tags html para hacer bing en otro tag
  $scope.trustedString = function(s){
      return $sce.trustAsHtml(s);
  };
  //genera una url confiable para reproducir el audio
  $scope.trustedURL = function(url){
    return $sce.trustAsResourceUrl(url);
  };

  //obtiene el feed del podscast
  $scope.readRSS = function() {
    console.log($scope.podcastUrl);
    //use

    var feed = new google.feeds.Feed($scope.podcastUrl,{
            api_key : 'xzs5k31qezyp5xultaxdamsdsafwse59xsvuuv7f',
            count : 10,
            //order_by : 'title',
            //order_dir : 'asc'
    });
      $scope.showResults = false;
      feed.load(function(result) {
        $scope.showResults=true;
        if (!result.error) {
          //console.log(result.feed.entries)
          $scope.podcasts = result.feed.entries;

        }
        else{
          $scope.podcasts=[];
          Materialize.toast("No se encontraron podcasts. Revisa la url y vuelve a intentarlo", 2000);
        }
        $scope.$apply();

      });
    };
  //
  // $scope.readyCallback = function () {
  //   Materialize.toast("Modal ready", 1000);
  // }
  $scope.setRss = function(url){
    $scope.podcastUrl= url;
  }

  $scope.completeCallback = function (selector) {
    console.log(selector)
    $('#'+selector)[0].pause();
    $('#'+selector)[0].currentTime = 0


    //Materialize.toast("Modal complete", 1000);
  }

});
